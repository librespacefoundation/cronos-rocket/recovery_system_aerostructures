# Recovery System
This repository contains every file associated with the rocket's recoverynt system.  

Cronos benefits from a dual event recovery system. What that means is that Cronos deploys a drogue parachute at apogee a main parachute at a lower altitude during its descent. Both parachutes are made of a lightweight, ripstop nylon fabric and are designed for the rocket to touchdown with a terminal velocity of about 5m/s. The parachutes are connected to each other and to the rocket itself via a complex recovery harness consisting of nylon webbing belts and various types of links. It is designed so that all all parts of the rocket are safely recovered.

## License  
The project is licensed under the [CERN OHL license](https://gitlab.com/librespacefoundation/cronos-rocket/mechanical-design/-/blob/master/LICENSE)
